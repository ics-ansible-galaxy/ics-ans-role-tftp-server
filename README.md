ics-ans-role-tftp-server
===================

Setup TFTP Server.

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

Role Variables
--------------

- ...

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - { role: ics-ans-role-tftp-server }
```

License
-------

BSD 2-clause
