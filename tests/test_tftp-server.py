import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_xinetd(Service):
    xinetd = Service("xinetd")
    assert xinetd.is_enabled
    assert xinetd.is_running
